#! /usr/bin/env python

import zmq
from struct import pack


def mk_message(command, value):
    return pack("ci", command, value)


def send(socket, command, value):
    msg = mk_message(command, value)
    socket.send(msg)

    #  Get the reply.
    message = socket.recv()
    return message


context = zmq.Context()

#  Socket to talk to server
print "Connecting to hello world server"
socket = context.socket(zmq.REQ)
socket.connect("tcp://localhost:5555")

#  Do 10 requests, waiting each time for a response
for request in range(10):
    message = send(socket, "A", 10)
    print "Received reply %s [ %s ]" % (request, message)

    message = send(socket, "M", 100)
    print "Received reply %s [ %s ]" % (request, message)

    message = send(socket, "M", -100)
    print "Received reply %s [ %s ]" % (request, message)

    message = send(socket, "B", 0)
    print "Received reply %s [ %s ]" % (request, message)


send(socket, "Q", 0)
