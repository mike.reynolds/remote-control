#! /usr/bin/env python

import zmq
from struct import unpack


def parse_message(msg):
    return unpack("ci", msg)


context = zmq.Context()
socket = context.socket(zmq.REP)
socket.bind("tcp://*:5555")

while True:
    #  Wait for next request from client
    message = socket.recv()

    command, value = parse_message(message)

    if "A" == command:
        print "angle: %d" % value

    elif "M" == command:
        print "motor: %d" % value

    elif "B" == command:
        print "break: %d" % value

    elif "Q" == command:
        print "quit: %d" % value
        socket.send(b"D")
        break

    #  Send reply back to client
    socket.send(b"D")